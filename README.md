# Kubernetes

![alt text](https://i.pinimg.com/originals/00/c7/18/00c71841fc9d2334f53b68a74877865c.png)


## Table of content 

- [Introduction](#Intro)

- [What is Kubernetes?](#wik)

- [Kubernetes Components ](#kc)

- [Installation](#INST) 

- [Pre-Requisite](#prerq)<br>

- [Cluster initialization](#ci) <br>

- [YAML](#yaml) <br>

- [Pods](#pods)<br>

- [Replication Controller VS ReplicaSet](#rsrc)

- [Deployments](#deploy)

- [Depolyments -Rollback and Update](#deploy2)

- [Layers of abstraction](#layers)

- [ConfigMap](#cm)

- [Secrets](#sec)

- [Kubernetes Services](#serv)

- [Kubernetes Ingress](#ing)

- [Namespaces](#ns)

- [Volumes](#vol)

- [Statefullset](#sfs)

- [Kubernetes with private docker Registry ](#kd)

<a name="Intro"></a>
### Introduction

In this project i will explain how you setup a kubernetes cluster from scratch and give you all needed components to manage it and expand it with practical YML files for each exsersise .

<a name="wik"></a>

## What is Kubernetes?

Kubernetes is an open-source container-orchestration system for automating application deployment, scaling, and management. It was
originally designed by Google, and is now maintained by the Cloud Native Computing Foundation . containerization with Kubernetes is a guranteed to acheive High Availbility,Scalebility and Diesaster recovery .

Kubernetes use Yaml configuration language for deploying apps and exposing internal and external services and almost evreything need to be accomplished  by writing a proper YAML file . 


<a name="kc"></a>

### Kubernetes Components

Kubernetes consist of Master node and a worker nodes or minion as they called in the past Master and worker  node should contain  a group of compnents  to setup the cluster in a proper way :

![comp](/uploads/e9fb01b11d5a5fa2c50bc9e771bdcffe/comp.png)

**Api Server:** Frontend for cluster and for Command Line Interface and manging the cluster  .

**etcd:** Contain a Key Value of each node to avoid conflict and include information about node resources .

**Kubelet:** Agent run on each node to assure that node is runing as expected .

**Container Run-time:** a container application such as docker 

**Schedueler:** Scheduelr role is to assign pods to the Nodes .

**Controller:** Controller is a brain for the cluster it knows when one container is fail and take a decision to create new one .

each node should have a container Run-time application (we will use docker in our labs ) ,Kubelet to interact with node,starting pods and assign resources from node to a pod another component need to be on each node is a kube-proxy to manage network hits and trying to avoid network over hit by trying to avoid if possible communicating with other pods in diffrent node .




<a name="prerq"></a>

### Pre-Requisite 

I will use a Kubeadm to setup my cluser Kubeadm is a utility to  setup multi nodes cluster on separate machines Kubeadm will help us to install all the requirements for worker and master nodes in an automation way so it will install all the compnents mentioned in previous section .

The kubeadm tool is good if you need:

- A simple way for you to try out Kubernetes, possibly for the first time.
- A way for existing users to automate setting up a cluster and test their application.
- A building block in other ecosystem and/or installer tools with a larger scope.


The below list of packages are required to be installed prior of Kubeadm installtion on A linux mint machine :-

- [x] 3 Virtual machines connected together and have a full network connectivity among all machines in the cluster. You can use either a public or a private network.

- [x] Linux Mint OS installed V.19.xxx or higher on all three machines .

- [x] Docker installed on all machines V.19.xxx or higher .

- [x] Minimum of  2 Gib of memory and 2 CPU cores (i recommend to use higher specification. ) 

- [x] Disaple all swap memory on all machines ==>
 ```bash  
 swapoff -a  
 ``` 

<a name="INST"></a>

### Installation setps 


```bash
sudo apt-get update && sudo apt-get install -y apt-transport-https curl
```
```bash
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
```
```bash
cat <<EOF | sudo tee /etc/apt/sources.list.d/kubernetes.list
deb https://apt.kubernetes.io/ kubernetes-xenial main
EOF
```
```bash
sudo apt-get update
```
```bash
sudo apt-get install -y kubelet kubeadm kubectl
```
```bash
sudo apt-mark hold kubelet kubeadm kubectl
```



- kubeadm: the command to bootstrap the cluster.

- kubelet: the component that runs on all of the machines in your cluster and does things like starting pods and containers.

- kubectl: the command line utility to talk with your cluster.



<a name="ci"></a>

### Cluster initialization

_Initializing your control-plane node_

The control-plane node is the machine where the control plane components run, including etcd (the cluster database) and the API Server (which the kubectl command line tool communicates with).

```js
kubeadm init <args> <options>
```

- Choose a Pod network add-on, and verify whether it requires any arguments to be passed to kubeadm init. Depending on which third-party provider you choose, you might need to set the --pod-network-cidr to a provider-specific value. Pod network is external network plugin to allow the communication between the pods in different nodes in this documentation we will use flanel plugin .container with helper in same pod can communicate directly and there is no need for external network as both are in same network space Flannel runs a small, single binary agent called flanneld on each host, and is responsible for allocating a subnet lease to each host out of a larger, preconfigured address space. Flannel uses either the Kubernetes API or etcd directly to store the network configuration. 

Lets start initiating our cluster :

![clus](/uploads/c286a41b2c0788d66cec210c5146248f/clus.png)

We have to use Kubeadm utility to initiliaze our cluster on a master node we have to apply the below command :

```bash 
kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=<MASTER NODE IP
```
**--pod-network-cidr=10.244.0.0/16** it is a required option for external network plugin that will specify the subnets of the pods among the nodes .

**--apiserver-advertise-address=<MASTER NODE IP>** it the IP-address for the api server runing for sure on master node (192.168.1.5)



After command is completed ( it may tike a litile while) you have to apply the below 

```bash
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

On Worker nodes

They have to join the cluster by using token generated by master when we initilaize it will show you the token after initiliazion command completed :

![int](/uploads/53acfaca9ec3431ce5a16e11a6a81af3/int.png)

```bash 
kubeadm join 192.168.8.100:6443 --token xxx --discovery-token-ca-cert-hash xxx
```
To delete one worker node from cluster :

```bash 
kubeadm reset
```

Now check if all nodes state are Ready :

``` bash 
kubectl get nodes
kubectl get nodes --watch  #for live command output 
```

![nodes](/uploads/3b96252542b227c4298b0cc3da675a49/nodes.png)

**The state of all nodes should be Ready **


<a name="yaml"></a>

### YAML

YAML is a human-readable data-serialization language. It is commonly used for configuration files and in applications where data is being stored or transmitted. Kubernetes depend on YAML language to deploy any application , so writing YAML file is required in Kubernetes you can install SCite or Visual studio IDE to help you in writing such files (i do highly recommend visual studion with kubernetes extension) .

![yaml1](/uploads/586315e119221c67cf7855a659a311d2/yaml1.png)

_Key Value Pair :_

Fundamentally, a file written in YAML consists of a set of key-value pairs.Each pair is written as key: value, where whitespace after the : is not optional. Key names in CWL files should not contain whitespace - for multi-word key names that have special meaning in the CWL specification and underscored key names otherwise. For example:

```yml
first_name: Bilbo
last_name: Baggins
age_years: 111
home:
Bag End, Hobbiton
```
The YAML above defines four keys - first_name, last_name, age_years, and home - with their four respective values. Values can be character strings, numeric (integer, floating point, or scientific representation), Boolean (true or false), or more complex nested types .
 Values may be wrapped in quotation marks but be aware that this may change the way that they are interpreted i.e. "1234" will be treated as a character string , while 1234 will be treated as an integer. This distinction can be important .

_Array :_
In certain circumstances it is necessary to provide multiple values or objects for a single key. As we’ve already seen in the “Mapped Objects” section above, more than one key-value pair can be mapped to a single key. However, it is also possible to define multiple values for a key without having to provide a unique key for each value. We can achieve this with an array, where each value is defined on its own line and preceded by
 For example:

```yaml
FILES:
- foo.txt
- bar.dat
- baz.txt
```

_Dictionary :_

Dictionary/Map – A more complex type of YAML file would be a Dictionary and Map .

![yaml1](/uploads/db0fa13f1f6ab210ffc9bd5465160654/yaml1.png)



<a name="pods"></a>

### PODS 

Kubernetes does not deploy the container directly into nodes but encapsulated in pod , a pod is a smallest option you can create in
Kubernetes . If you need to scale up your node we will build new pod the relation between pods and container is 1:1 , in simple words if we want to scale up the the cluster we deploy new pod if we want to scale it down we will remove pod we do not deploy 2 containers in one pod unless some special cases The one pod can have multiple containers when ever you have helper container you can not let the application live without the helper containers inside the pod can directly communicate together as they have the same network space .

![pods](/uploads/fba965cf84ec9ffe95664b965fd07ad9/pods.png)

**PODS WITH YAML :**
Each Deployment on Kubernetes has main fields in YAML file those fields are the main structure for any deployments in Kubernetes :

- apiversion: Which version of the Kubernetes API you're using to create this object.
- kind : What kind of object you want to create.
- metadata: Data that helps uniquely identify the object, including a name string, UID, and optional namespace.
- spec : contains nested fields specific to that object .

apiVersion and Kind field depends on what type of obects you want to create each kind has a specific apiversion as per the below table :

| Kind | ApiVersion |
| ------ | ------ |
| Pod | v1 |
| Replicaset | apps/v1 |
| Deployment | apps/v1 |
| Service | v1 |

Example_pod1:

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: myapp
  labels:
    name: myapp
spec:
  containers:
   - name: apache
     image: httpd
```             
Example_pod2:

_Passing Enviroment variable with pod_


```yaml
apiVersion: v1
kind: Pod
metadata:
 name: postgres
 labels:
  tier: db-tier
spec:
 containers:
 - name: postgres
   image: postgres
   env:
   - name: POSTGRES_PASSWORD
     value: mysecretpassword
```

To deploy this pod apply on the master this command :

```bash
kubectl apply -f xxx.yml
#OR
kubectl create -f xxx.yml
```

To list all pods in cluster 
```bash
kubectl get pods 
```
To list all  pods in cluster with more information 
```bash
kubectl get pods -o wide 
```
To get verbose information about pod
```bash
kubectl describe pod <NAME OF POD > 
```
To see the logs from inside the pod
```bash
kubect logs <NAME OF POD > 
```

To log in inside the pod 

```bash
kubect -exec -it  <NAME OF POD > /bin/bash 
```

<a name="rsrc"></a>

### Replication Controller VS ReplicaSet :

Replica set and replication controller - Both the terms have the word replica. Why do we need to replicate anything? Let's start with that. There are multiple ways your container can crash. Replication is used for the core purpose of Reliability, Load Balancing, and Scaling.There are two main types of Replications in Kubernetes - Replica sets and Replication controller.ReplicaSet or ReplicaController used to deploy many pods at one time like if I want to deploy 6 pods on 3 different nodes at the same time to guarantee that the service will be available even if one pod is down .

one of the major roles of replicas on Kubernetes is to monitor the pods within the replica and make sure to keep desire number of pods always running and re-deploy pods in case of crashing and that is one of the biggest reason behind micro-service technology .

Replica-controller is an old technology replaced by Replica-Set all production systems are using Replica-Set currently Replica Set has A selector field which is mandatory while it is optional in replica controllerthe main purpose of the selector field to monitor all the pods that match the label described in the selector field and even take into the consideration the pod that built before deploying the replica set and calculate them as a pods in the replica set scope .

The replication controller supports equality based selectors whereas the replica set supports equality based as well as set based selectors.

**Equality based Selectors:**

Equality based selectors allow filtering by label keys and values. Matching objects must satisfy all of the specified label constraints, though they may have additional labels as well. 

Three operators used in set based equality based selectors are _= , == , !=_. The first two represent equality (and are simply synonyms), while the latter represents inequality. 

For example, if we provide the following selectors:
```yaml
env = prod
tier != frontend
```
Here, we’ll select all resources with key equal to environment and value equal to production. The latter selects all resources with key equal to tier and value distinct from frontend, and all resources with no labels with the tier key.

One usage scenario for equality-based label requirement is for Pods to specify node selection criteria.

**Set Based Selectors:**

Unlike Equality based, Set-based label selectors allow filtering keys according to a set of values.

Three kinds of operators used in set-based selectors are _in , notin, exists_(only the key identifier).

For example, if we provide the following selectors:
```yaml
env in (prod, qa)
tier notin (frontend, backend)
```
Here, in the first selector will selects all resources with key equal to environment and value equal to production or qa.
The second example selects all resources with key equal to tier and values other than frontend and backend, and all resources with no labels with the tier key. The third example selects all resources including a label with key partition; no values are checked.

The set-based label selector is a general form of equality since environment=production is equivalent to environment in (production). Similarly for != and notin.



Replicaset and replica controller have almost the same structure of YAML the below images shows the structure of replica controller :

![rsrc](/uploads/6c8d1f972c10c267a8207917933af546/rsrc.png)

Example_ReplicaSet:

```yaml
apiVersion: apps/v1
kind: ReplicaSet
metadata: 
  name: nginx-rs
spec:
  replicas: 3
  selector:
    matchLabels:
      env: prod
    matchExpressions:
    - { key: tier, operator: In, values: [frontend] }
  template:
    metadata:
      name: nginx
      labels: 
        env: prod
        tier: frontend
    spec:
      containers:
      - name: nginx-container
        image: nginx
        ports:
        - containerPort: 80
```



We modify the number of replicas among the nodes by specifying number in  replicas field under spec section .

Example_ReplicationController:

```yaml
apiVersion: v1
kind: ReplicationController
metadata: 
  name: nginx-rc
spec:
  replicas: 3
  selector:
    app: nginx
    tier: dev
  template: 
    metadata:
      name: nginx
      labels: 
        app: nginx
        tier: dev
    spec:
      containers:
      - name: nginx-container
        image: nginx
        ports:
        - containerPort: 80
```



In replicas section we specify number 6 which means i need 6 pods createed among all nodes but what if there is already 4 pods created before the ReplicationController applied ?!! ReplicationController will create only 2 pods to keep the desired number to 6 

![Labels](/uploads/06d1dc0fc2ac947dab91ad87c12b0acd/Labels.png)


To deploy  ReplicaSet or ReplicationController  apply on the master this command :

```bash
kubectl apply -f xxx.yml
#OR
kubectl create -f xxx.yml
```

To list all ReplicaSet in cluster 
```bash
kubectl get replicaset 
#OR
kubectl get rs 
```
To list all ReplicationController in cluster 
```bash
kubectl get rc
```
To list all  ReplicaSet in cluster with more information 
```bash
kubectl get rs -o wide 
```
To list all  ReplicationController in cluster with more information 
```bash
kubectl get rc -o wide 
```

To get verbose information about ReplicaSet 
```bash
kubectl describe rs <NAME OF rs > 
```
To get verbose information about ReplicaController 
```bash
kubectl describe rc <NAME OF rc > 
```

**Scaling Replicas in Kubernetes : **

- Manually change the replicas field inside the file of replica set YAML and apply the below command :

```bash
kubectl replace -f <File .yml>
```

- Directly by CLI :

```bash
kubectl scale –replicas=<numner of replicas > -f <file .YML>
#OR
kubectl scale –replicas=<numner of replicas > replicaset <replica-set-name OR ReplicationController name >
```

<a name="deploy"></a>

### Deployments

For example you have web server you need to deploy on a production you need not one but many such instance of the web servers for obvious reason secondly whenever newer version of application become availble in docker registry you would like to upgrade your docker instance simultaneously however upgrade your instances you do not upgrad all at once to allow users to still use the application this kind of upgrade call rolling update upgrade .Suppose one of the upgrade you did lead to unexpected issue or error you
would like to rollback to the old versions all of these capabilities are existing in Kubernetes Deployments . Deployment give you a benifits of K8S features for rolling up upgrade on all production we do not apply pods or ReplicaController only deployments  

![deploy](/uploads/5602c693134ffbc2ab18929cd4bec583/deploy.png)

The content of deployment file is exactly the similar of the replica set except for the Kind .

Example_Deployment1:

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: pim
spec:
  replicas: 4
  selector:
    matchLabels:
      name: pim

  template:

    metadata:
     name: pim
     labels:
       name: pim
    spec:
     containers:
      - name: pim
        image: httpd
```
Example_Deployment2:

_Passing Enviroment variable with deployment_

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.7.9
        env:
        - name: MY_VAT
          value: MY_VALUE
        ports:
        - containerPort: 80
```
Example_Deployment3:

_Spercify the entrypoint with  deployment._

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    run: nginx
  name: nginx-deploy
spec:
  replicas: 2
  selector:
    matchLabels:
      run: nginx-main
  template:
    metadata:
      labels:
        run: nginx-main
    spec:
      containers:
      - name: httpd
        image: httpd
        command: ["/bin/sh"]
        args: ["-c", "while true; do echo hello; sleep 10;done"]
```                               
<a name="deploy2"></a>



To deploy  Deployment  apply on the master this command :

```bash
kubectl apply -f xxx.yml
#OR
kubectl create -f xxx.yml
```

To list all Deployment in cluster 
```bash
kubectl get Deployment 
#OR
kubectl get Deployment 
```

To list all  Deployment in cluster with more information 
```bash
kubectl get Deployment -o wide 
```


To get verbose information about ReplicaSet 
```bash
kubectl describe Deployment  <NAME OF Deployment > 
```

### Depolyments -Rollback and Update :

When you first deploy the application it will roll-out and create revision in the future when the application is upgraded it will trigger new roll-out and new revision this give us the luxury to rollback to previous revisions .

You can see the status and history of the roll-out by apply the below :

```bash
kubectl rollout history deployment/<DEPLYMENT NAME>
kubectl rollout status deployment/<DEPLYMENT NAME>
```

There is 2 types of deployment strategy :

1.   recreate : destroy all containers and build new containers and pods with the newer version which is cause application outages and downtime .

![re](/uploads/22934025e62eed216f461e9cb5cabd01/re.png)

2. Rolling Update : upgrade the containers simultaneously and there is no downtime The rolling update is the default deployment startegy .

![re](/uploads/c6afc6f25c793c3e03cf27c484d116a0/re.png)

You can apply the rolling update by changing the yaml file manually and apply the below :

```bash
kubectl apply -f <deployment.yml>
```
or you can pass the new image within the command as below :

```bash
kubectl set image deployment/<deplyment name> nginx:naginx1:9:9
```
To check the history of Deployment
```bash
kubectl rollout history deployment/<Deployment Name>
```
To Rollback the  Deployment
```bash
kubectl rollout undo deployment/<Deployment Name>
```

Rolling update will create new replica set and generate the new pods inside it and make the pods inside the old replica set to avoid application outages

![re](/uploads/49420b06bcdb8f88ea43f982be164a24/re.png)

<a name="layers"></a>

### Layers of abstraction 

After finishing Pods,ReplicaController and Deployment the layers of abstarction will be as beow diagram :

![i](/uploads/5dddc6ecea021b37d2bd87db7169e0bb/i.png)
<a name="config"></a>

### Config Map :

ConfigMap IS AN external configuration for you application inside the pod so you can controll the configration from outside the Pod by applying a ConfigMap Kubernetes object to keep containerized applications portable and standard for all deployments Pods. 

**How to use and define your ConfigMap inside your deployment :**

Example_ConfigMap1:

In This example we will create a ConfigMap that conatins apache configration and share it among all pods in all nodes .

1. Building ConfigMap

```bash
kubectl create configmap <Name> --from-file=httpd.conf=httpd.conf
```

verify that ConfigMap is created as expected :

```bash
kubectl get cm 
```

2. Pass the config map inside all pods in deployment :

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpd
spec:
  replicas: 2
  selector:
    matchLabels:
      name: httpd

  template:
    metadata:
      labels:
        name: httpd
    spec:
      containers:
        - name: httpd
          image: httpd
          volumeMounts:
            - name: httpd
              mountPath: /usr/local/apache2/conf
      volumes:
        - name: httpd
          configMap:
            name: httpd
```


Now check the conetnt of httpd.conf :

```bash 
kubectl exec -it <pod name> /bin/bash 
cat /usr/local/apache2/conf/httpd.conf
```
Example_ConfigMap2:

In This example we will create a ConfigMap that pass a value of enviroment variable  inside the httpd container   and share it among all pods in all nodes .

1. Create the ConfigMap 

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: httpdenv
data:
  httpd_db_url: db.local
```

2. Build Deployment Yaml file that have enviroment variable that take the value from the ConfigMap 

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: httpd
  labels:
    app: httpd
spec:
  replicas: 2
  selector:
    matchLabels:
      app: httpd
  template:
    metadata:
      labels:
        app: httpd
    spec:
      containers:
      - name: httpd
        image: httpd
        env:
        - name: httpd_db_url
          valueFrom:
              configMapKeyRef:
                name: httpdenv
                key: httpd_db_url

```

![httpd](/uploads/a83afad6a843c1a3979ee761da18cbf0/httpd.png)

As above the enviroment variable httpd_db_url is created and filled with the proper data that we specified in ConfigMap file .

As we noticed in the example there is to way to create configmap by manually or building the Yaml file configMap object is used apiversion 'v1' .

<a name="config"></a>

### Secrets in Kubernetes :

Kubernetes Secrets let you store and manage sensitive information, such as passwords, OAuth tokens, and ssh keys. Storing confidential information in a Secret is safer and more flexible than putting it verbatim in a Pod definition or in a container image. 

Example_Secrets:

In this Example we will try to pass the MongoDb username and password in a secret file as it is a sensitive information 

1. Build the secret file 

```yaml
apiVersion: v1
kind: Secret
metadata:
  name: mongodb-secret
type: Opaque
data:
  mongo-root-username: IHlheg==
  mongo-root-password: IHlheg==
```
Secrets need the values in 64 encoded format to apply it in Linux write the below on any terminal :

```bash 
echo -n "<DATA YOU WANT TO ENCODE>" | base64
```

2. Create The deployment to read the data from the secret file 

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: mongodb-deployment
  labels:
    app: mongodb
spec:
  replicas: 2
  selector:
    matchLabels:
      app: mongodb
  template:
    metadata:
      labels:
        app: mongodb
    spec:
      containers:
      - name: mongodb
        image: mongo



        resources:
          limits:
            memory: "128Mi"
            cpu: "500m"
        ports:
        - containerPort: 27017
        env:
          - name: MONGO_INITDB_ROOT_USERNAME
            valueFrom:
              secretKeyRef:
                name: mongodb-secret
                key:  mongo-root-username
          - name: MONGO_INITDB_ROOT_PASSWORD
            valueFrom:
              secretKeyRef:
                name: mongodb-secret
                key: mongo-root-password
```

<a name="serv"></a>

### Kubernetes Services :

Like a pod, a Kubernetes service is a REST object. A service is a both an abstraction that defines a logical set of pods and a policy for accessing the pod set by provide a set of pods based on its labels ans delector a static IP address to acces the pods internaly and externaly , services in kubernetes provide a Loadbalancer feature by itself .

1. NodePort : to reach any port inside the pod we need to deploy a service and the client can communicate with service and it will forward the request to the target port inside the pod the below image describe that :

![s](/uploads/78193cbe3153a6992c7d56060573fde0/s.png)

Example_Nodeport:

```yaml
apiVersion: v1

kind: Service

metadata:

  name: httpd

  labels:

    name: httpd

spec:

  type: NodePort

  ports:

    - port: 80

      targetPort: 80

      nodePort: 30002

  selector:

   name: httpd
```                  

The above Yaml will allow you to reach your pod by reaching your worker node IP with port 30002 , nodePort if not specified it will generate a random port number within 30000-32767 .

2. ClusterIp : To give all the pods that have specific selector and allow internal communications between the pods without using specific IP as it may be on of the pods crashes and another pod with different IP created .

Example_ClusterIp:

```yaml
apiVersion: v1

kind: Service

metadata:

  name: httpd

  labels:

    name: httpd

spec:

  type: ClusterIp

  ports:

    - port: 80

      targetPort: 80


  selector:

   name: httpd
```

The above Yaml will allow you to grant  internal communication between pods in diffrent nodes with one IP  .


3. Headless:

It is used when you want to communicate directly with pod and bypass  the service it is widely used in statefullset deployment as the pods are not identcal .

Example_Headless:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: rabbitmq-headless
  namespace: test-rabbitmq
spec:
  clusterIP: None
  ports:
  - name: httpd
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: httpd
```
specifying "None" for the cluster IP 

4. Loadbalancer:

It is same is NodePort Service but in Loadbalancer service the server become accesible by cloud Loadbalancer .


To deploy  Services  apply on the master this command :

```bash
kubectl apply -f xxx.yml
#OR
kubectl create -f xxx.yml
```

To list all Services in cluster 
```bash
kubectl get services 
```

To list all  services in cluster with more information 
```bash
kubectl get services -o wide 
```


To get verbose information about ReplicaSet 
```bash
kubectl describe services  <NAME OF Service > 
```


<a name="ing"></a>

### Kubernetes Ingress

An API object that manages external access to the services in a cluster, typically HTTP. Ingress may provide load balancing, SSL termination and name-based virtual hosting.

Kubernetes Ingress is a very important topic to avoid client to reach you cluster  directly  with a Nodeport service which is  not recommended at all to use it in production enviroment while the kubernetes ingress will be the only entrypoint for the cluster and it will forward the cluster to the proper internal service inside it (Cluster Ip service) .

![s](/uploads/8a4502f01f2dac2a436ff11acf87e149/s.png)

Kubernetes Ingress has 2 main components :

1. Ingress resources: 

Traffic routing is controlled by rules defined on the Ingress resource.

2. Ingress controller :

An Ingress controller is responsible for fulfilling the Ingress, usually with a load balancer, though it may also configure your edge router or additional frontends to help handle the traffic.
An Ingress does not expose arbitrary ports or protocols. Exposing services other than HTTP and HTTPS to the internet typically uses a service of type Service.Type=NodePort or Service.Type=LoadBalancer.

Steps to apply Kubernetes Ingress :

1. Install K8S ingress controllers

Let’s clone the nginx ingress controller.

```bash
 git clone https://github.com/nginxinc/kubernetes-ingress/
 cd kubernetes-ingress/deployments
 git checkout v1.10.1
```
Create a namespace and a service account for the Ingress controller:

```bash
cd kubernetes-ingress/deployments
kubectl apply -f common/ns-and-sa.yaml
```

Create a secret with a TLS certificate and a key for the default server in NGINX. If you have your own certificates you can use them here. I’m going to use the self-signed cert:

```bash
kubectl apply -f common/default-server-secret.yaml 
```
Create the NGINX ConfigMap

```bash
kubectl apply -f common/nginx-config.yaml 
```

RBAC applied The RBAC API declares four kinds of Kubernetes object: Role, ClusterRole, RoleBinding and ClusterRoleBinding.

```bash
kubectl apply -f rbac/ap-rbac.yaml
```
We’ve laid the ground work for the controller and now we’re ready to deploy it. You can choose to deploy it as a Deployment or a DaemonSet. I’ll be choosing to deploy as a DaemonSet.

> Kubernetes deployments manage stateless services running on your cluster (as opposed to for example StatefulSets which do manage stateful services). ... However, DaemonSets attempt to adhere to a one-Pod-per-node model, either across the entire cluster or a subset of nodes.

```bash
kubectl apply -f daemon-set/nginx-ingress.yaml
```

Create an IngressClass resource (for Kubernetes >= 1.18):

> Ingresses can be implemented by different controllers, often with different configuration. Each Ingress should specify a class, a reference to an IngressClass resource that contains additional configuration including the name of the controller that should implement the class.




```bash
kubectl apply -f common/ingress-class.yaml
```
Verify That evreything is Ok 

```bash
kubectl get ns
kubectl get ingress
kubectl get pods --namespace nginx-ingress -o wide
```

![s](/uploads/a0686b572d29b98b2b54e9d37d43f77e/s.png)

2. Create our deployment


```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: pim
spec:
  replicas: 4
  selector:
    matchLabels:
      name: pim

  template:

    metadata:
     name: pim
     labels:
       name: pim
    spec:
     containers:
      - name: pim
        image: httpd
```

3. Expose container port via internal clusterIP service 

```yaml
apiVersion: v1
kind: Service
metadata:
  name: pim-internal-service
spec:
  selector:
    name: pim
  ports:
  - port: 80
    targetPort: 80
```

4. Create Ingress resources 

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: myapp-ingress
  annotations:
    kubernetes.io/ingress.class: nginx

spec:
  rules:
  - host: myapp.com
    http:
      paths:

      - backend:
            serviceName: pim-internal-service
            servicePort: 80
```         
> IN ingress resource yaml file we specify  the name of the clusteIP service in serviceName field .

Now you have to add on /etc/hosts on all worker nodes myapp.com to map its IP :

```bash
echo x.x.x.x myapp.com >> /etc/hosts
```

![s](/uploads/6c589621d847f260b9acce4bafe3e12d/s.png)


**Sticky session in K8S ingress :**

Nginx ingress controller already has these requirement considered and implemented. The ingress controller replies the response with a Set-Cookie header to the first request. The value of the cookie will map to a specific pod replica. When the subsequent request come back again, the client browser will attach the cookie and the ingress controller is therefore able to route the traffic to the same pod replica.

Example_Ingress_Sticy-session:

```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: nginx-test
  annotations:
    nginx.ingress.kubernetes.io/affinity: "cookie"
    nginx.ingress.kubernetes.io/session-cookie-name: "route"
    nginx.ingress.kubernetes.io/session-cookie-expires: "172800"
    nginx.ingress.kubernetes.io/session-cookie-max-age: "172800"

spec:
  rules:
  - host: stickyingress.example.com
    http:
      paths:
      - backend:
          serviceName: http-svc
          servicePort: 80
        path: /
```

<a name="ns"></a>

### Namespaces 

Kubernetes supports multiple virtual clusters backed by the same physical cluster. These virtual clusters are called namespaces .

By default there is 4 Name spaces :

```bash
kubectl get namespace
#OR
kubectl get ns
```
- Kube-system : system process for kubectl .
- kube-public : contain configmap thats include cluster information .
- kube-node-lease : it contains information about nodes heartbeats .
- default : name space that used when you did not specify a specific namespace .

To create new name space  :

```bash
kubectl create ns <ns name> 
```

specify it inside your yaml 
```yaml
'
'
'
metadata:
  name: xxxx
  namespace: xxxxx
  ```

  or by cli 

  ```bash
  kubectl apply -f xxx.yml  -n=xxx
  ```

  All get command need to specify with it a namespace if any :

  ```bash
  kubectl get pods --namecpace=xxx -o wide 
  kubectl get services --namecpace=xxx -o wide 
  kubectl get deployment --namecpace=xxx -o wide 
  ```
**Why namespace is important ?**

1. Oraganize application that are not connecting together 
2. avoid conflict of depolying files with same name .
3. resource sharing
4. blue/green deployment
5. limit resources

You can not access resources from diffrent namespaces secrets,configmap,pods and services are not accessible between namespaces, nodes and volumes can not be under any namespace .

> if you want to call a service from other namespace it should be written like this servicename.namespace 

<a name="vol"></a>

### Volumes in Kubernetes

Volumes in kubernetes are physical storage pressistance locally in the nodes or remotely volumes does not depend on pod lifecycle  so when the pod crash for any reason the new pod can catch the stored data from the volumes. 
Kubernetes supports many types of volumes. A Pod can use any number of volume types simultaneously.

**Storage must be availble and reacheable by all nodes .**



**Types of Volumes**

- emptyDir − It is a type of volume that is created when a Pod is first assigned to a Node. It remains active as long as the Pod is running on that node. The volume is initially empty and the containers in the pod can read and write the files in the emptyDir volume. Once the Pod is removed from the node, the data in the emptyDir is erased.

- hostPath − This type of volume mounts a file or directory from the host node’s filesystem into your pod.

- gcePersistentDisk − This type of volume mounts a Google Compute Engine (GCE) Persistent Disk into your Pod. The data in a gcePersistentDisk remains intact when the Pod is removed from the node.

- awsElasticBlockStore − This type of volume mounts an Amazon Web Services (AWS) Elastic Block Store into your Pod. Just like gcePersistentDisk, the data in an awsElasticBlockStore remains intact when the Pod is removed from the node.

- nfs − An nfs volume allows an existing NFS (Network File System) to be mounted into your pod. The data in an nfs volume is not erased when the Pod is removed from the node. The volume is only unmounted.

- iscsi − An iscsi volume allows an existing iSCSI (SCSI over IP) volume to be mounted into your pod.

- flocker − It is an open-source clustered container data volume manager. It is used for managing data volumes. A flocker volume allows a Flocker dataset to be mounted into a pod. If the dataset does not exist in Flocker, then you first need to create it by using the Flocker API.

- glusterfs − Glusterfs is an open-source networked filesystem. A glusterfs volume allows a glusterfs volume to be mounted into your pod.

- rbd − RBD stands for Rados Block Device. An rbd volume allows a Rados Block Device volume to be mounted into your pod. Data remains preserved after the Pod is removed from the node.

- cephfs − A cephfs volume allows an existing CephFS volume to be mounted into your pod. Data remains intact after the Pod is removed from the node.

- gitRepo − A gitRepo volume mounts an empty directory and clones a git repository into it for your pod to use.

- secret − A secret volume is used to pass sensitive information, such as passwords, to pods.


- downwardAPI − A downwardAPI volume is used to make downward API data available to applications. It mounts a directory and writes the requested data in plain text files.

- azureDiskVolume − An AzureDiskVolume is used to mount a Microsoft Azure Data Disk into a Pod.

**Persistent Volume and Persistent Volume Claim :**

Persistent Volume (PV): it needs actual storage ( localdisk,ceohs,nfs..etc)

Persistent Volume Claim (PVC):  The storage requested by Kubernetes for its pods is known as PVC. The user does not need to know the underlying provisioning. The claims must be created in the same namespace where the pod is created.

![s](/uploads/4f0c2c10f6ffc7eccbfa01300b719622/s.png)
**_pv is not under any namespace_**

When creating PV you have to specify in Yaml 3 important things:

1. How much storage
2. access
3. backend parameters

```yaml
kind: PersistentVolume ---------> 1
apiVersion: v1
metadata:
   name: pv0001 ------------------> 2
   labels:
      type: local
spec:
   capacity: -----------------------> 3
      storage: 10Gi ----------------------> 4
   accessModes:
      - ReadWriteOnce -------------------> 5
      hostPath:
         path: "/tmp/data01" --------------------------> 6
```

kind: PersistentVolume → We have defined the kind as PersistentVolume which tells kubernetes that the yaml file being used is to create the Persistent Volume.

name: pv0001 → Name of PersistentVolume that we are creating.

capacity: → This spec will define the capacity of PV that we are trying to create.

storage: 10Gi → This tells the underlying infrastructure that we are trying to claim 10Gi space on the defined path.

ReadWriteOnce → This tells the access rights of the volume that we are creating.

path: "/tmp/data01" → This definition tells the machine that we are trying to create volume under this path on the underlying infrastructure.

Example_LocalVolume:

Creating PV

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: task-pv-volume
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/tmp"
```


Creating PVC

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: task-pv-claim
spec:
  storageClassName: manual
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 3Gi
```

Attached with Pod 

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: task-pv-pod
spec:
  volumes:
    - name: task-pv-storage
      persistentVolumeClaim:
        claimName: task-pv-claim
  containers:
    - name: task-pv-container
      image: httpd
      ports:
        - containerPort: 80
          name: "http-server"
      volumeMounts:
        - mountPath: "/tmp"
          name: task-pv-storage

```

Attached with Deployment

```yaml 
apiVersion: apps/v1
kind: Deployment
metadata:
  name: pim
spec:
  replicas: 4
  selector:
    matchLabels:
      name: pim

  template:

    metadata:
     name: pim
     labels:
       name: pim
    spec:
       volumes:
       - name: task-pv-storage
         persistentVolumeClaim:
          claimName: task-pv-claim
       containers:
       - name: task-pv-container
         image: httpd
         ports:
         - containerPort: 80
         volumeMounts:
          - mountPath: "/tmp"
            name: task-pv-storage
```

To deploy  a presistanceVolume or persistentVolumeClaim   apply on the master this command :

```bash
kubectl apply -f xxx.yml
#OR
kubectl create -f xxx.yml
```

To list all presistanceVolume in cluster 
```bash
kubectl get pv 
```
To list all presistanceVolumeClaim in cluster 
```bash
kubectl get pvc 
```

To list all  presistanceVolume in cluster with more information 
```bash
kubectl get pv -o wide 
```
To list all  presistanceVolumeClaim in cluster with more information 
```bash
kubectl get pvc -o wide 
```
To get verbose information about presistanceVolume 
```bash
kubectl describe pv  <NAME OF presistanceVolume > 
```
To get verbose information about presistanceVolumeClaim 
```bash
kubectl describe pv  <NAME OF presistanceVolumeClaim > 
```

**Storage Class :**

A fetaure in K8S to allow storage volume created on demand before this feature adminstarator  have to create a pv from a storage  before deployment as a pre-request Storage class is an abstraction layer underlaying storage platform.

<a name="sfs"></a>

### StateFullset 

There is 2 Type of apps  statefull applications and stateless applications all databases are statefull applications and all apps that stored data permanently on another hand  statless app does not keep record state and treat each request as a new request .
statefull app use statefullset object for deplyment while statless app use deployment object for deployment both statfullset and deployment  use same concept for storage and volumes both required pv and pvc and local or remote storage and both are replicate the pods among the nodes SO WHAT IS THE DIFFRENCE ?!!

![s](/uploads/1e421a74affc58ce747238756cbad32e/s.png)

Deployment is easy to configure and create  pods in a random names with hashes  while statefullset pods have a static identity and names in statfullset pods can not be created or deleted at same time  it starts removing pods starting from the higher identity pods in statefullset are not identical there will be a master pods and worker pods , master pods are responsible for writing data on a storage while worker systncing its data from master pods but the question came here why statfullset need to have pods with sticky in identity the reason taht if one of master pod or worker pods die the new one should have the same role and name to act as the old one to avoid data inconstiency .

Worker and master pods use seperate PV but the storage in master pod should be accessible by all other worker pods so they can replicate the data from it thats why a remote storage is used in a master pod as the local storage only accessible by the node itself .

When new pod create it replicate the data from previous identity pod (newpodidentity -1 ) thats why the previous pod shoud be runing .

Pods in statefullset app have a DNS name for each one 

> Statefull application are not recommended in Containeriaztion applications .

<a name="kd"></a>

### Kubernetes with private docker Registry 

**Private registry in Docker :**

The Registry is a stateless, highly scalable server side application that stores and lets you distribute Docker image.
You should use the Registry if you want to:
- tightly control where your images are being stored.
- fully own your images distribution pipeline .
- integrate image storage and distribution tightly into your in-house development workflow.

**STEPS:**

Install openssl 

```bash
apt-get -y install openssl

```
Add the following with your Machine specific IP address under the section [ v3_ca ] in the file placed in /etc/pki/tls/openssl.cnf

```js
[ v3_ca ]
subjectAltName=IP:IP_ADDRESS_OF_YOUR_VM
```




Create a self signed certificate 

```bash
mkdir -p /certificates
cd certificates
openssl req \
  -newkey rsa:4096 -nodes -sha256 -keyout domain.key \
  -x509 -days 365 -out domain.crt
  
#Enter all required fields (it could be anything) but please enter your Server IP address when it prompts for -> Common Name (e.g. server FQDN or YOUR name)

Common Name (e.g. server FQDN or YOUR name) []: IP_ADDRESS_OF_YOUR_VM

# Check if the certificates are created in the current directory (certificates)

ls
```



Install docker private registry image and run it 

```docker 
sudo docker run -d -p 5000:5000 --restart=always --name registry \
  -v /certificates:/certificates \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certificates/domain.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certificates/domain.key \
  registry:2
  
```

To verify our Docker registry, let us pull a small hello-world docker image from Docker-Hub registry, tag it appropriately and try to push it to our local Registry.

Docker Server does not trust the self-signed certificate and our certificates need to be manually added to Docker Engine

```bash
sudo mkdir -p /etc/docker/certs.d/IP_ADDRESS_OF_YOUR_VM:5000

#Please make sure to copy domain.crt as ca.crt (or rename later on) 
sudo cp /certificates/domain.crt /etc/docker/certs.d/IP_ADDRESS_OF_YOUR_VM:5000/ca.crt

sudo ls /etc/docker/certs.d/IP_ADDRESS_OF_YOUR_VM:5000/

#reload docker daemon to use the ca.crt certificate
sudo service docker reload
```


```bash
docker pull hello-world
docker tag hello-world IP_ADDRESS_OF_YOUR_VM:5000/hello-world

docker push IP_ADDRESS_OF_YOUR_VM:5000/hello-world
```

On Client 

Add the following with your Machine specific IP address under the section [ v3_ca ] in the file placed in /etc/pki/tls/openssl.cnf

```js
[ v3_ca ]
subjectAltName=IP:IP_ADDRESS_OF_YOUR_VM
```
Take the certificate from registry server .

```bash
sudo mkdir -p /etc/docker/certs.d/IP_ADDRESS_OF_YOUR_REGISTRY:5000

#Please make sure to copy domain.crt as ca.crt (or rename later on) 
sudo cp /certificates/domain.crt /etc/docker/certs.d/IP_ADDRESS_OF_YOUR_REGISTRY:5000/ca.crt

sudo ls /etc/docker/certs.d/IP_ADDRESS_OF_YOUR_REGISTRY:5000/

#reload docker daemon to use the ca.crt certificate
sudo service docker reload
```
**Kubernetes With Private Registry :**

A Kubernetes cluster uses the Secret of docker-registry type to authenticate with a container registry to pull a private image
First of all Kubernetes needs you to authenticate your private registry with username and password and there is a lot of methods to do that .

Sign up on docker hub 

[https://hub.docker.com/](url)

```kubectl
kubectl create secret docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email>
```

- <your-registry-server> is your Private Docker Registry FQDN. Use https://index.docker.io/v2/ for DockerHub.
- <your-name> is your Docker username.
- <your-pword> is your Docker password.
- <your-email> is your Docker email.


to check

```bash
kubectl get regcred
```

![s](/uploads/99f2f0742430ef4a42b7f7b50ba3b4d3/s.png)

To understand the contents of the regcred Secret you just created, start by viewing the Secret in YAML format:

```bash
kubectl get secret regcred --output=yaml
```

The output is similar to this:

```js
apiVersion: v1
kind: Secret
metadata:
  ...
  name: regcred
  ...
data:
  .dockerconfigjson: eyJodHRwczovL2luZGV4L ... J0QUl6RTIifX0=
type: kubernetes.io/dockerconfigjson
```

The value of the .dockerconfigjson field is a base64 representation of your Docker credentials.

To understand what is in the .dockerconfigjson field, convert the secret data to a readable format:
```bash
kubectl get secret regcred --output="jsonpath={.data.\.dockerconfigjson}" | base64 --decode
```

The output is similar to this:

```js
{"auths":{"10.123.41.148:5000":{"username":"yazeed.smadi","password":"xxxxxx","email":"sm_smd@yahoo.com","auth":"eWF6Z...z"}}}
```

To understand what is in the auth field, convert the base64-encoded data to a readable format:

```bash
echo "ewef...cdrg" |base64 --decode
```

The output, username and password concatenated with a :, is similar to this:

```js
yazeedsmadi:xxxxxxxxxxx
```

  Example_Pod-Private_registry:

  Create a Pod that uses your Secret 
```yaml

apiVersion: v1
kind: Pod
metadata:
  name: private-reg
spec:
  containers:
  - name: private-reg-container
    image: <your-private-image>
  imagePullSecrets:
  - name: regcred
  ```


  Example_Deployment-Private_registry:

  Create a Deployment that uses your Secret 

  ```yaml
  apiVersion: apps/v1
kind: Deployment
metadata:
  name: pim
spec:
  replicas: 4
  selector:
    matchLabels:
      name: pim

  template:

    metadata:
     name: pim
     labels:
       name: pim
    spec:
     containers:
      - name: pim
        image: <your-private-image>
        command: ["/bin/sh"]
        args: ["-c", "while true; do echo hello; sleep 10;done"]

     imagePullSecrets:
     - name: regcred
```
